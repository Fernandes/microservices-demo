# Copyright 2021 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# https://mcr.microsoft.com/v2/dotnet/sdk/tags/list


FROM mcr.microsoft.com/dotnet/sdk:5.0.302-buster-slim-arm64v8 as builder

# add debian backports to get a fresh grpc plugin
RUN echo "deb http://deb.debian.org/debian buster-backports main" | tee /etc/apt/sources.list.d/backports.list

RUN apt-get update && apt-get install -y gnupg2

# add debian keys to use backports
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 7638D0442B90D010
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 04EE7237B7D453EC

# install protoc+plugin
RUN apt update
RUN apt install -y -t buster-backports protobuf-compiler-grpc

# tell dotnet to use the local protoc
RUN export PROTOBUF_PROTOC=/usr/bin/protoc








WORKDIR /app
COPY cartservice.csproj .
RUN dotnet restore cartservice.csproj -r linux-arm64

#RUN ls -la ~/.nuget/
#RUN ls -la ~/.nuget/packages

# trick dotnet to use the local grpc plugin as there are no environment variable to help
RUN ln -s /usr/bin ~/.nuget/packages/grpc.tools/2.38.1/tools/linux_

COPY . .
# Fix the issue on Debian 10: https://github.com/dotnet/dotnet-docker/issues/2470
ENV COMPlus_EnableDiagnostics=0
RUN dotnet publish cartservice.csproj -p:PublishSingleFile=true -r linux-arm64 --self-contained true -p:PublishTrimmed=True -p:TrimMode=Link -c release -o /cartservice --no-restore

# https://mcr.microsoft.com/v2/dotnet/runtime-deps/tags/list
#FROM mcr.microsoft.com/dotnet/runtime-deps:5.0.8-alpine3.13-arm64v8
FROM mcr.microsoft.com/dotnet/runtime-deps:5.0.8-focal-arm64v8
RUN apt update
RUN apt install -y curl
RUN GRPC_HEALTH_PROBE_VERSION=v0.3.6 && \
     curl https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-arm64 -o /bin/grpc_health_probe && \
    chmod +x /bin/grpc_health_probe
WORKDIR /app
COPY --from=builder /cartservice .
ENV ASPNETCORE_URLS http://*:7070
ENTRYPOINT ["/app/cartservice"]